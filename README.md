# Desafio Focare 1 #

O desafio é desenvolver um programa que capture a idade e o sexo, adicione em uma lista de classe 'ListaCliente()', e através dessa lista adicione em um ListView em ordem crescente. Com um filtro para filtrarmos apenas os masculinos e os femininos.

### Entrada ###

* Idade;
* Sexo.

** A classe deverá conter estas duas variaveis.

### Etapas ###

* Capturar o valor da Idade (inteiro);
* Capturar o sexo (varchar ex.: M ou F);
* Adicionar em uma lista de classe com estas variaveis acima.
* Adicionar em um ListView
* Colocar 2 CheckBox, uma sendo masculino e outro feminino que ao clicar ira ser feito o filtro no ListView

### Saída ###

* ListView filtrado;

### Requisitos técnicos ###

O desafio deve ser feito em C# Forms ou VB Forms. 

### Entrega ###

A entrega deve ser feita por e-mail aos endereços abaixo com o código do desafio ou link gerado no compilador online utilizado na resolução.

- daniel@focare.net.br
- victor@focare.net.br